import pandas as pd
import numpy as np
import seaborn as sb
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.cluster import DBSCAN
from sklearn.metrics import davies_bouldin_score
from util import distribution_characteristics, plot_histogram, print_formatted_clusters

def plot_DBSCAN(eps, df, x, y, min_samples, ax, palette='Set1'):
    model = DBSCAN(eps=eps, min_samples=min_samples).fit(df)
    ax.set_title(x + ' ' + y + ' clustered scatter plot')
    sb.scatterplot(x=x, y=y, hue=model.labels_, palette=palette, data=df, ax=ax)
    return model

def estimate_epsilon(df, min_samples, step, starting, ax):
    db = {}
    for i in range(2, 10):
        curr_eps = starting + i*step
        model = DBSCAN(eps=curr_eps, min_samples=min_samples).fit(df)
        db[curr_eps] = davies_bouldin_score(X=df, labels=model.labels_)

    ax.set_xlabel('Epsilon values')
    ax.set_ylabel('Davies-Bouldin score')
    ax.set_title('Davies-Bouldin score for DBSCAN clustering')
    ax.plot(list(db.keys()), list(db.values()), marker='o')
    

def main():

    data_df = pd.read_csv('data/Country-data.csv')
    countries = data_df.iloc[:, 0]
    data_no_country = data_df.drop(['country'], axis=1)

    x_lab = 'gdpp'
    y_lab = 'health'
    df = data_no_country[[x_lab, y_lab]]

    distribution_characteristics(df, x_lab)
    distribution_characteristics(df, y_lab)

    _, axs = plt.subplots(1,2)
    plot_histogram(df=df, x_lab=x_lab, ax=axs[0], unit='')
    plot_histogram(df=df, x_lab=y_lab, ax=axs[1], unit='% of GDP per capita')
    plt.show()

    min_max_scaler = MinMaxScaler()
    df = min_max_scaler.fit_transform(X=df)
    df = pd.DataFrame(data=df, columns=[x_lab, y_lab])

    _, axs = plt.subplots(1,2)
    min_samples = 4
    estimate_epsilon(df=df, min_samples=min_samples, step=0.002, starting=0.027, ax=axs[0])
    model = plot_DBSCAN(df=df, x=x_lab, y=y_lab, eps=0.041, min_samples=min_samples, ax=axs[1])
    print_formatted_clusters(countries=countries, labels=model.labels_, n=5)
    plt.show()
    
main()