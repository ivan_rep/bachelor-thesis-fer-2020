from sklearn.mixture import GaussianMixture
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sb
from util import print_formatted_clusters

def estimate_using_bic_and_aic(df):
    bic, aic = {}, {}
    models = []
    for i in range(1, 10):
        model = GaussianMixture(n_components=i, n_init=100, init_params='kmeans').fit(X=df)
        model.predict(X=df)
        bic[i] = model.bic(X=df)
        aic[i] = model.aic(X=df)
        models.append(model)

    _, axs = plt.subplots(1, 2)
    axs[0].plot(list(bic.keys()), list(bic.values()), marker='o')
    axs[0].set_ylabel('BIC score')
    axs[0].set_xlabel('Number of components')
    axs[0].set_title('Bayesian information criterion plot')

    axs[1].plot(list(aic.keys()), list(aic.values()), marker='o')
    axs[1].set_ylabel('AIC score')
    axs[1].set_xlabel('Number of components')
    axs[1].set_title('Akaike information criterion plot')

    return models

def gmm_cluster_plot(df, labels, ax, x, y, title):
    ax.set_xlabel(x)
    ax.set_ylabel(y)
    ax.set_title(title)
    sb.scatterplot(x=x, y=y, hue=labels, palette='Set1', data=df, ax=ax)

def main():
    data_df = pd.read_csv('data/Country-data.csv')
    countries = data_df.iloc[:, 0]
    data_no_country = data_df.drop(['country'], axis=1)
    
    x = 'health'
    y = 'total_fer'

    df = data_no_country[[x, y]]

    models = estimate_using_bic_and_aic(df=df)
    plt.show()

    _, axs = plt.subplots(1, 2)
    labels2 = models[2].predict(df)
    labels4 = models[4].predict(df)
    gmm_cluster_plot(df=df, labels=labels2, ax=axs[0], x=x, y=y, title='Gaussian mixture model clustering with 3 components')    
    gmm_cluster_plot(df=df, labels=labels4, ax=axs[1], x=x, y=y, title='Gaussian mixture model clustering with 5 components')
    plt.show()

    print_formatted_clusters(countries=countries, labels=labels2, n=5)
    print_formatted_clusters(countries=countries, labels=labels4, n=5)

main()