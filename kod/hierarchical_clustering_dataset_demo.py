import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sb
from sklearn.cluster import AgglomerativeClustering
from sklearn.metrics import davies_bouldin_score, silhouette_score, calinski_harabasz_score
from sklearn.preprocessing import MinMaxScaler
from util import distribution_characteristics, plot_histogram, print_formatted_clusters

def estimate_n_clusters(df, ax, title, x, y, f, linkage):
    d = estimate_using_f(df=df, f=f, linkage=linkage)
    ax.set_xlabel(x)
    ax.set_ylabel(y)
    ax.set_title(title)
    ax.plot(list(d.keys()), list(d.values()), marker='o')

def estimate_using_f(df, f, linkage):
    d = {}
    for i in range(2, 15):
        model = create_hierarchical_clustering_model(df=df, n_clusters=i, linkage=linkage).fit(df)
        d[i] = f(X=df, labels=model.labels_)
    return d

def create_hierarchical_clustering_model(df, n_clusters, linkage):
    model = AgglomerativeClustering(n_clusters=n_clusters, linkage=linkage).fit(df)
    return model

def plot_hierarhical_clustering(x, y, labels, df, ax, palette='Set1'):
    sb.scatterplot(x=x, y=y, hue=labels, data=df, ax=ax, palette=palette)
    ax.set_title('Clustered scatter plot with n_clusters='+str(len(set(labels))))

def main():
    data_df = pd.read_csv('data/Country-data.csv')
    countries = data_df.iloc[:, 0]
    df = data_df.drop(['country'], axis=1)

    x = 'income'
    y = 'inflation'
    df = df[[x, y]]

    distribution_characteristics(df=df, var_name=x)
    distribution_characteristics(df=df, var_name=y)

    _, axs = plt.subplots(1, 2)
    plot_histogram(df=df, x_lab=x, ax=axs[0], unit='')
    plot_histogram(df=df, x_lab=y, ax=axs[1], unit='% of GDP')
    plt.show()

    min_max_scaler = MinMaxScaler()
    df = min_max_scaler.fit_transform(X=df)
    df = pd.DataFrame(data=df, columns=[x, y])

    x_lab = 'Number of clusters'
    _, axs = plt.subplots(1, 2)
    estimate_n_clusters(df=df, ax=axs[0], title='Silhouette score for HAC', y='Silhouette score', x=x_lab, f=silhouette_score, linkage='ward')
    estimate_n_clusters(df=df, ax=axs[1], title='Davies-Bouldin index for HAC', y='Davies-Bouldin index', x=x_lab, f=davies_bouldin_score, linkage='ward')
    plt.show()


    _, axs = plt.subplots(1, 2)
    model1 = create_hierarchical_clustering_model(df=df, n_clusters=4, linkage='ward')
    print_formatted_clusters(countries=countries, labels=model1.labels_, n=5)
    plot_hierarhical_clustering(x=x, y=y, labels=model1.labels_, df=df, ax=axs[0], palette='Set1')


    model2 = create_hierarchical_clustering_model(df=df, n_clusters=8, linkage='ward')
    print_formatted_clusters(countries=countries, labels=model2.labels_, n=5)
    plot_hierarhical_clustering(x=x, y=y, labels=model2.labels_, df=df, ax=axs[1], palette='Set1')
    plt.show()

    print(calinski_harabasz_score(X=df, labels=model1.labels_))
    print(calinski_harabasz_score(X=df, labels=model2.labels_))

    print_formatted_clusters(countries=countries, labels=model1.labels_, n=5)
    print_formatted_clusters(countries=countries, labels=model2.labels_, n=5)

main()