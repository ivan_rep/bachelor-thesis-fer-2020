import numpy as np
import pandas as pd
import seaborn as sb
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
from yellowbrick.cluster import SilhouetteVisualizer, KElbowVisualizer
from sklearn.metrics import silhouette_score
from util import distribution_characteristics, plot_histogram, print_formatted_clusters

def elbow_plot(df, ax, title, metric='distortion'):
    model = KMeans()
    visualizer = KElbowVisualizer(model, k=(2,10), metric=metric, ax=ax, timings=False)   
    visualizer.fit(df)
    ax.set_ylabel(title + ' score')
    ax.set_xlabel('k')
    ax.set_title(title + ' Score Elbow for KMeans Clustering')

def create_k_means_model(df, k):
    model = KMeans(n_clusters=k)
    model = model.fit(df)
    return model

def silhoutte_score_plot(df, model, ax, palette):
    visualizer = SilhouetteVisualizer(estimator=model, colors=palette, ax=ax)
    visualizer.fit(X=df)
    visualizer.finalize()
    ax.set_title('Silhouette score plot')
    print('Silhouette score is: '+str(silhouette_score(X=df, labels=model.labels_)))

def scatterplot(x_lab, y_lab, data, ax, title, palette, model):
    ax.set_title(title+' clustered scatter plot')
    sb.scatterplot(x=x_lab, y=y_lab, hue=model.labels_, data=data, ax=ax, palette=palette)

def main():

    data_df = pd.read_csv('data/Country-data.csv')
    countries = data_df.iloc[:, 0]
    data_no_country = data_df.drop(['country'], axis=1)

    x_lab = 'life_expec'
    y_lab = 'exports'
    df = data_no_country[[x_lab, y_lab]]

    distribution_characteristics(df, x_lab)
    distribution_characteristics(df, y_lab)

    _, axs = plt.subplots(1, 2)
    plot_histogram(df=df, x_lab=x_lab, ax=axs[0], unit='years')
    plot_histogram(df=df, x_lab=y_lab, ax=axs[1], unit='% of GDP per capita')
    plt.show()

    _, axs = plt.subplots(1, 2)
    elbow_plot(df=df, ax=axs[0], title='Distortion')
    elbow_plot(df=df, ax=axs[1], title='Calinski Harabasz', metric='calinski_harabasz')
    plt.show()

    cmap = get_cmap('tab10')
    palette = [cmap(i) for i in range(5)]

    _, axs = plt.subplots(1, 2)
    model = create_k_means_model(df=df, k=4)
    print_formatted_clusters(countries=countries, labels=model.labels_, n=5)
    scatterplot(x_lab=x_lab, y_lab=y_lab, model=model, palette=palette[:4], data=df, ax=axs[0], title=x_lab.capitalize()+' '+y_lab)
    silhoutte_score_plot(df=df, model=model, ax=axs[1], palette=palette[:4])
    plt.show()

    _, axs = plt.subplots(1, 2)
    model = create_k_means_model(df=df, k=5)
    print_formatted_clusters(countries=countries, labels=model.labels_, n=5)
    scatterplot(x_lab=x_lab, y_lab=y_lab, model=model, palette=palette, data=df, ax=axs[0], title=x_lab.capitalize()+' '+y_lab)
    silhoutte_score_plot(df=df, model=model, palette=palette, ax=axs[1])
    plt.show()

main()