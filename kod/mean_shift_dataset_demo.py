import pandas as pd
import seaborn as sb
import matplotlib.pyplot as plt
from sklearn.cluster import MeanShift, estimate_bandwidth
from sklearn.metrics import davies_bouldin_score, silhouette_score
from util import distribution_characteristics, plot_histogram, print_formatted_clusters

def estimate_bandwidth_davies_bouldin_score(df, ax):
    db = estimate_using_f(df=df, f=davies_bouldin_score)
    ax.set_xlabel('Bandwidth')
    ax.set_ylabel('Davies-Bouldin index')
    ax.set_title('Davies-Bouldin index for mean shift clustering')
    ax.plot(list(db.keys()), list(db.values()), marker='o')

def estimate_bandwidth_silhouette(df, ax):
    ss = estimate_using_f(df=df, f=silhouette_score)
    ax.set_xlabel('Bandwidth')
    ax.set_ylabel('Silhouette score')
    ax.set_title('Silhouette score for mean shift clustering')
    ax.plot(list(ss.keys()), list(ss.values()), marker='o')

def estimate_using_f(df, f, starting=12, step=1):
    d = {}
    for i in range(1, 15):
        curr_bandwidth = starting + step*i
        model = MeanShift(bandwidth=curr_bandwidth).fit(df)
        d[curr_bandwidth] = f(X=df, labels=model.labels_)
    return d

def create_mean_shift_model(df, bandwidth=None):
    if(bandwidth is None):
        estimate = estimate_bandwidth(X=df)
        model = MeanShift(bandwidth=estimate)
        print('Estimated bandwidth is: {0}'.format(estimate))
    else:
        model = MeanShift(bandwidth=bandwidth)
    model = model.fit(df)
    return model

def plot_mean_shift(model, df, x, y, ax, palette='Set1'):
    sb.scatterplot(x=x, y=y, hue=model.labels_, palette=palette, data=df, ax=ax)
    ax.set_title(x + ' ' + y + ' clustered scatter plot')

def main():

    data_df = pd.read_csv('data/Country-data.csv')
    countries = data_df.iloc[:, 0]
    data_no_country = data_df.drop(['country'], axis=1)

   
    x_lab = 'total_fer'
    y_lab = 'child_mort'
    df = data_no_country[[x_lab, y_lab]]

    distribution_characteristics(df, x_lab)
    distribution_characteristics(df, y_lab)

    _, axs = plt.subplots(1,2)
    plot_histogram(df=df, x_lab=x_lab, ax=axs[0], unit='number of children born to each woman')
    plot_histogram(df=df, x_lab=y_lab, ax=axs[1], unit='‰ deaths under 5 years of age')
    plt.show()
    
    _, axs = plt.subplots(1, 2)
    estimate_bandwidth_davies_bouldin_score(df=df, ax=axs[0])
    model = create_mean_shift_model(df=df, bandwidth=19).fit(df)
    plot_mean_shift(model=model, df=df, x=x_lab, y=y_lab, ax=axs[1])
    print_formatted_clusters(countries=countries, labels=model.labels_, n=5)
    plt.show()

    _, axs = plt.subplots(1, 2)
    estimate_bandwidth_silhouette(df=df, ax=axs[0])
    model = create_mean_shift_model(df=df).fit(df)
    plot_mean_shift(model=model, df=df, x=x_lab, y=y_lab, ax=axs[1])
    print_formatted_clusters(countries=countries, labels=model.labels_, n=5)
    plt.show()

main()