import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
from sklearn.datasets import fetch_california_housing
from sklearn.cluster import DBSCAN
import numpy as np
import pandas as pd

def create_basemap():
    m = Basemap(projection='mill',
                llcrnrlat=32,
                llcrnrlon=233,
                urcrnrlat=43,
                urcrnrlon=247,
                resolution='i')
    m.drawcoastlines(linewidth=1, zorder=1)
    m.drawmapboundary(fill_color='lightblue')
    m.fillcontinents(color='white',lake_color='lightblue')

    m.drawcountries()
    m.drawcounties()
    # m.drawrivers()

    return m

def main():
    df = fetch_california_housing(as_frame=True).data
    x_lab = 'Longitude'
    y_lab = 'Latitude'
    df = df[[x_lab, y_lab]]
    df[x_lab] += 360

    model = DBSCAN(eps=0.3, min_samples=85)
    model.fit(df)
    m = create_basemap()
    x, y = m(df[x_lab], df[y_lab])
    l = m.scatter(x=x, y=y, marker='o', c=model.labels_, s=3, cmap='Set1_r', zorder=2)
    plt.xlabel(x_lab)
    plt.ylabel(y_lab)
    plt.title('Clustered California housing dataset')
    plt.show()

main()