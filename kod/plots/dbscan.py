from distance_measures import euclidean_distance
import matplotlib.pyplot as plt

class DBSCAN:

    def __init__(self, eps, min_pts):
        self.eps = eps
        self.min_pts = min_pts
        self.clusters = []
        self.noise = []
        self.listeners = []

    def add_listener(self, l):
        self.listeners.append(l)

    def notify_listeners(self, c):
        [l.update(c) for l in self.listeners]

    def range_query(self, data, p1):
        return [p2 for p2 in data if euclidean_distance(p1.values, p2.values) < self.eps]
    
    def group(self, data):
        noise = []
        clusters = []
        for point in data:
            if (not point.visited):
                point.set_visited()
                neighbours = self.range_query(data, point)
                if (len(neighbours) < self.min_pts):
                    point.set_in_group()
                    noise.append(point.values)
                    continue
                
                c = []
                point.set_in_group()
                self.fill_cluster(c, point, neighbours, data)
                self.clusters.append(c)
        self.noise = noise
            
    def fill_cluster(self, c, point, neighbours, data):
        c.append(point.values)

        for p in neighbours:
            if (not p.visited):
                p.set_visited()
                p_neighbours = self.range_query(data, p)

                if(len(p_neighbours) >= self.min_pts):
                    neighbours.extend(p_neighbours)
            if (not p.in_group):
                p.set_in_group()
                c.append(p.values)
            
            self.notify_listeners(c[:])


class Point:
    
    def __init__(self, values):
        self.values = values
        self.visited = False
        self.in_group = False

    def set_visited(self):
        self.visited = True

    def set_in_group(self):
        self.in_group = True


class TwoByThreePlotListener:

    def __init__(self, iters):
        self.iters = iters
        self.iters_copy = iters
        self.clusters = []
        self.asdf = 0
    
    def update(self, c):
        self.iters -= 1
        if(self.iters == 0):
            self.clusters.append(c)
            self.iters = self.iters_copy

    def plot_iters(self, data):
        col = 3
        row = 1
        fig, axs = plt.subplots(row, col)
        self.clusters = self.clusters[-(col*row):]
        colors = ['red', 'black']

        # 1d plots
        for j in range(col):
            axs[j].axis('equal')
            #axs[j].legend(loc='upper left')
            axs[j].scatter([p.values[0] for p in data], [p.values[1] for p in data], s=15, color=colors[1])
            axs[j].scatter([p[0] for p in self.clusters[j]], [p[1] for p in self.clusters[j]], s=15, color=colors[0])
            axs[j].set_xlabel('x')
            axs[j].set_ylabel('y')
            #axs[j].legend(('Not clustered yet', 'Cluster 1'), scatterpoints=1, loc='upper left', ncol=1, fontsize=8)

        # 2d plots
        # for i in range(row):
        #    for j in range(col):
        #        axs[i, j].axis('equal')
        #        axs[i, j].legend(loc='upper left')
        #        axs[i, j].scatter([p.values[0] for p in data], [p.values[1] for p in data], s=5, color=colors[1])
        #        axs[i, j].scatter([p[0] for p in self.clusters[i*col+j]], [p[1] for p in self.clusters[i*col+j]], s=5, color=colors[0])
        #        axs[i, j].legend(('Not clustered yet', 'Cluster 1'), scatterpoints=1, loc='upper left', ncol=1, fontsize=8)
