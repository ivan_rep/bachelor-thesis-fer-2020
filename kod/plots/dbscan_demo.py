import numpy as np
import matplotlib.pyplot as plt
from distance_matrix import distance_matrix
from sklearn.datasets import make_circles
from dbscan import Point, DBSCAN, TwoByThreePlotListener
import math

def main():
    np.random.seed(42)
    x, y = make_circles(n_samples=750, noise=0.06, random_state=2, factor=0.5)
    draw_circle(pts=x)
    plt.axis('equal')
    plt.show()

    data = [Point(list(elem)) for elem in x]
    model = DBSCAN(eps=0.15, min_pts=3)
    listener = TwoByThreePlotListener(iters=3600)
    model.add_listener(listener)
    model.group(data=data)
    listener.plot_iters(data=data)
    plt.show()

    colors = ['red', 'blue', 'black']
    draw_clusters_and_noise(model=model, colors=colors)
    plt.legend(('Cluster 1', 'Cluster 2', 'Outliers'), scatterpoints=1, loc='upper left', ncol=1, fontsize=8)
    plt.axis('equal')
    plt.show()

def draw_clusters_and_noise(model, colors):
    for i, cluster in enumerate(model.clusters):
        x = [elem[0] for elem in cluster]
        y = [elem[1] for elem in cluster]
        plt.scatter(x, y, color=colors[i], s=10)
    x = [elem[0] for elem in model.noise]
    y = [elem[1] for elem in model.noise]

def draw_circle(pts):
    x = [elem[0] for elem in pts]
    y = [elem[1] for elem in pts]
    plt.scatter(x, y, color="black", s=10)

def generate_circular_points(r,n):
    return [ (math.cos(2*math.pi/n*x)*r+np.random.normal(-30,30),
        math.sin(2*math.pi/n*x)*r+np.random.normal(-30,30)) for x in range(1, n+1) ]
        

main()