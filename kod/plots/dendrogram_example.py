from scipy.cluster.hierarchy import dendrogram, linkage
import matplotlib.pyplot as plt
import numpy as np

def main():
    data = [[1, 4], [1.5,3.7], [1.5,3.1], [2.6, 2]]

    fig, axs = plt.subplots(1,2)
    labels = range(0, 4)
    for i in range(len(data)):
        axs[0].annotate(text=labels[i], xy=(data[i][0], data[i][1]), xytext=(-8, 5), textcoords='offset points', ha='right', va='top')
    axs[0].scatter(x=[p[0] for p in data], y=[p[1] for p in data], s=40)

    
    l = linkage(data, 'centroid')
    dendrogram(l, orientation='top', distance_sort='descending', show_leaf_counts=True, ax=axs[1])
    plt.show()

main()