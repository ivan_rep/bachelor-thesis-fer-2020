from distance_measures import euclidean_distance

class distance_matrix:

    def __init__(self, data):
        l = len(data)
        self.m = [[None for i in range(l)] for j in range(l)]
        self.dim = l

    def find_distances(self, data):
        l = len(data)
        for i in range(l):
            for j in range(i, l):
                if(i == j):
                    self.m[i][j] = 0
                else:
                    distance = euclidean_distance(data[i], data[j])
                    self.m[i][j] = distance
                    self.m[j][i] = distance


