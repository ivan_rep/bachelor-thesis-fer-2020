def euclidean_distance(first, second):
    ret_val = 0
    for i, elem in enumerate(first):
        ret_val += (first[i]-second[i])**2
    return ret_val**(1/2)

def manhattan_distance(first, second):
    ret_val = 0
    for i, elem in enumerate(first):
        ret_val += abs(first[i]-second[i])
    return ret_val