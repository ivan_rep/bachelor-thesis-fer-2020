from sklearn.mixture import GaussianMixture
import numpy as np
import matplotlib.pyplot as plt
# import matplotlib as mpl
# from scipy import linalg
from matplotlib.patches import Ellipse
import matplotlib.transforms as transforms

# def plot_results(x, y, means, covariances, colors):
#     x = np.array(x)
#     y = np.array(y)
#     canvas = plt.subplot(1, 1, 1)
#     for i, (mean, covar, color) in enumerate(zip(means, covariances, colors)):
#         v, w = linalg.eigh(covar)
#         v = 2*np.sqrt(2)*np.sqrt(v)
#         u = w[0]/linalg.norm(w[0])

#         if not np.any(y == i):
#             continue
#         plt.scatter(x[y==i, 0], x[y == i, 1], s=10, color=color)

#         angle = np.arctan(u[1]/u[0])
#         angle = 180*angle/np.pi
#         ell = mpl.patches.Ellipse(mean, v[0], v[1], 180+angle, color=color)
#         ell.set_clip_box(canvas.bbox)
#         ell.set_alpha(0.5)
#         canvas.add_artist(ell)
#         canvas.scatter(x=mean[0], y=mean[1], color='black')

def confidence_ellipse(x, y, ax, n_std=3.0, facecolor='none', **kwargs):

    cov = np.cov(x, y)
    pearson = cov[0, 1]/np.sqrt(cov[0, 0] * cov[1, 1])
    ell_radius_x = np.sqrt(1 + pearson)
    ell_radius_y = np.sqrt(1 - pearson)
    ellipse = Ellipse((0, 0), width=ell_radius_x * 2, height=ell_radius_y * 2,
                      facecolor=facecolor, **kwargs)

    scale_x = np.sqrt(cov[0, 0]) * n_std
    mean_x = np.mean(x)

    scale_y = np.sqrt(cov[1, 1]) * n_std
    mean_y = np.mean(y)

    transf = transforms.Affine2D() \
        .rotate_deg(45) \
        .scale(scale_x, scale_y) \
        .translate(mean_x, mean_y)

    ellipse.set_transform(transf + ax.transData)
    return ax.add_patch(ellipse)

def main():
    m1 = [5,5]
    m2 = [10, 10]
    ms = [m1, m2]
    cov1 = [[1, 0], [0, 1]]
    cov2 = [[0.7, 1], [1, 0.7]]

    x1, y1 = np.random.multivariate_normal(m1, cov1, 200).T
    x2, y2 = np.random.multivariate_normal(m2, cov2, 200).T

    pts = []
    [pts.append([p[0], p[1]]) for p in zip(x1, y1)]
    [pts.append([p[0], p[1]]) for p in zip(x2, y2)]

    n = 2
    gmm = GaussianMixture(n_components=n, covariance_type='full').fit(pts)
    y_pred = gmm.predict(pts)

    c1 = []
    c2 = []
    for i, p in enumerate(pts):
        if (y_pred[i] == 0):
            c1.append(p)
        elif (y_pred[i] == 1):
            c2.append(p)
    clusters = []
    clusters.append(c1)
    clusters.append(c2)

    ax = plt.subplot(1,1,1)
    colors = ['cornflowerblue', 'darkorange']
    for (c, m, color) in zip(clusters, ms, colors):
        x = [p[0] for p in c]
        y = [p[1] for p in c]
        confidence_ellipse(x=x, y=y, ax=ax, edgecolor='red')
        ax.scatter(x=x, y=y, c=color, s=15)
    
    for m in ms:
        ax.scatter(x=m[0], y=m[1], c='black', s=20)

    plt.axis('equal')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.show()


main()