import numpy
from distance_matrix import distance_matrix

class agglomerative_hierarchical_clustering:

    def __init__(self, dmatrix, data):
        self.dmatrix = dmatrix
        self.clusters = data

    def calculate_clusters(self, cluster_num, data):
        clusters = self.clusters

        while(len(clusters) != cluster_num):
            i_min, j_min = self.average_linkage(len(clusters))

            clusters[i_min] = clusters[i_min] + clusters[j_min]
            clusters.pop(j_min)

            new_points = [[mean(cluster[::2]), mean(cluster[1::2])] for cluster in clusters]

            new_dm = distance_matrix(new_points)
            new_dm.find_distances(new_points)
            self.dmatrix = new_dm

        self.clusters = clusters
        return self.clusters


    def average_linkage(self, dim):
        if(dim < 2):
            return

        i_min = 0
        j_min = 1

        matrix = self.dmatrix.m
        minimum = matrix[i_min][j_min]

        for i in range(dim):
            for j in range(i+1, dim):
                if(matrix[i][j] < minimum):
                    minimum = matrix[i][j]
                    i_min = i
                    j_min = j
                    
        return i_min, j_min

def mean(l):
    return sum(l)/len(l)
