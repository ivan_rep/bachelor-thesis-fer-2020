from distance_matrix import distance_matrix
import matplotlib.pyplot as plt
from hierarchical_clustering import agglomerative_hierarchical_clustering
from matplotlib.axes import Axes

def find_changed_index(c_new, c_old):
    index = -1
    for i in range(len(c_new)):
        if(c_new[i] != c_old[i]):
            return i
    return index

def main():
    data = [[1, 4], [1.5,3.7], [1.5,3.1], [2.6, 2]]
    colors = ['blue', 'orange', 'green', 'red']

    m = distance_matrix(data)
    m.find_distances(data)
    model = agglomerative_hierarchical_clustering(m, data)
    
    iters = len(data)

    row = 2
    col = 2
    fig, axs = plt.subplots(row, col)
    z = 0
    k = 0

    c_old = data[:]
    nums = list(range(0, 6))

    for i in range(iters, 0, -1):
        c_new = model.calculate_clusters(i, data)

        if(i != iters):
            index = find_changed_index(c_new, c_old)
            c_old = c_new[:]
            colors.pop(index+1)
            nums.pop(index+1)

        axs[z, k].axis('equal')
        for j in range(len(c_new)):
            axs[z, k].scatter(model.clusters[j][::2], model.clusters[j][1::2], color=colors[j], s=50)
        
        axs[z, k].set_xlabel('x')
        axs[z, k].set_ylabel('y')

        k += 1
        if(k == col):
            z += 1
            k = 0
    plt.show()

main()