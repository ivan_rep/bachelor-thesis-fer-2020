import numpy as np
import random
from distance_measures import euclidean_distance

def mean(l):
    return list(sum(np.array(l))/len(l))

class k_means:

    def __init__(self, k):
        self.k = k

    def generate_centroids(self, data):
        self.centroids = []
        coords = random.sample(range(0, len(data)), self.k)
        for coord in coords:
            self.centroids.append(data[coord])

    def set_centroids(self, centroids):
        self.centroids = centroids

    def empty_clusters(self): 
        self.clusters = [[] for i in range(self.k)]

    def calculate_centroids(self, data):
        for i, p in enumerate(data):
            min_dist = euclidean_distance(p, self.centroids[0])
            k = 0

            for j in range(1, self.k):
                min_candidate = euclidean_distance(p, self.centroids[j])
                if (min_candidate < min_dist):
                    min_dist = min_candidate
                    k = j
          
            self.clusters[k].append(p)

        for i in range(self.k):
            if (len(self.clusters[i])!=0):
                self.centroids[i] = mean(self.clusters[i])
