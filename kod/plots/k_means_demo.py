from k_means import k_means
from matplotlib.axes import Axes
from sklearn.datasets import make_blobs
import matplotlib.pyplot as plt
import numpy as np


def main():
    centers = [(3.5, 3), (4, 2), (2, 3)]
    cluster_std = [0.4, 0.3, 0.4]

    x, y = make_blobs(n_samples=100, cluster_std=cluster_std, centers=centers, n_features=3, random_state=1)

    data = [elem.tolist() for elem in x]
    model = k_means(k=3)
    model.set_centroids([[2.3, 1],[2, 4],[5,3]])
    # model.generate_centroids(data=data)
    model.empty_clusters()

    offset = 1 
    row = 2
    col = 2
    iters = row * col + offset
    fig, axs = plt.subplots(row, col)
    z = 0
    k = 0

    for i in range(iters):
        model.calculate_centroids(data=data)
        c = model.clusters[:]
        model.empty_clusters()

        if(offset > 0):
            offset -= 1
            continue

        axs[z, k].axis('equal')

        # plot points
        for j in range(model.k):
            axs[z, k].scatter([p[0] for p in c[j]], [p[1] for p in c[j]], s=20)

        # plot centroids
        for j in range(model.k):
            axs[z, k].scatter(model.centroids[j][0], model.centroids[j][1], color='black', s=40)
            

        # axs[z, k].legend(('Cluster 1', 'Cluster 2', 'Cluster 3', 'Centroid'), scatterpoints=1, loc='upper left', ncol=1, fontsize=8)
        axs[z, k].set_xlabel('x')
        axs[z, k].set_ylabel('y')

        k += 1
        if(k == col):
            z += 1
            k = 0
    plt.show()

main()