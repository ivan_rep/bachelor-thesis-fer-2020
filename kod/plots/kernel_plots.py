import matplotlib.pyplot as plt
from math import exp

def set_plot_specifics(title, d, ax):
    ax.plot(list(d.keys()), list(d.values()), linewidth=2.5)
    ax.set_title(title)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.yaxis.grid(color='gray', linestyle='dashed')
    ax.xaxis.grid(color='gray', linestyle='dashed')

def main():
    start, end, step = (-3, 3, 0.01)
    flat, gaussian = {}, {}
    i = 0
    temp = start + i*step
    while(temp < end):
        flat[temp] = 1 if 1 >= abs(temp) else 0
        gaussian[temp] = exp(-temp**2)
        i += 1
        temp = start + i*step

    _, axs = plt.subplots(1,2)
    set_plot_specifics('Unit flat kernel', flat, axs[0])
    set_plot_specifics('Gaussian kernel', gaussian, axs[1])
    plt.show()

main()