from distance_measures import euclidean_distance
import numpy as np

def mean(l):
    return list(sum(np.array(l))/len(l))

class MeanShiftSimulation:

    def __init__(self, bandwidth):
        self.centroids = []
        self.bandwidth = bandwidth
        self.first_iter = False

    def init_centroids(self, data):
        for i, p in enumerate(data):
            self.centroids.append(p)
    
    def grouping_iteration(self, data):
        curr_centroids = self.centroids
        new_centroids = []
        for i, c in enumerate(curr_centroids):
            lt_bandwidth = []

            for j, p in enumerate(data):
                if (euclidean_distance(c, p) < self.bandwidth):
                    lt_bandwidth.append(p)

            if (len(lt_bandwidth) != 0):
                new_centroids.append(mean(lt_bandwidth))
            else:
                new_centroids.append(c)

        self.centroids = new_centroids

