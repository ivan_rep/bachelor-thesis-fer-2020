from sklearn.datasets import make_blobs
from mean_shift import MeanShiftSimulation
import matplotlib.pyplot as plt

def main():
    centers = [[5, 3], [12, 5], [5, 11]]
    X, _ = make_blobs(n_samples=500, centers=centers, cluster_std=1.25)
    data = []
    [data.append(p) for p in X.tolist()]

    ms = MeanShiftSimulation(bandwidth=3)
    ms.init_centroids(data=data)

    row = 2
    col = 2
    _, axs = plt.subplots(row, col)
    for i in range(row):
        for j in range(col):
            x = [p[0] for p in ms.centroids]
            y = [p[1] for p in ms.centroids]
            axs[i][j].scatter(x=x, y=y, s=15)
            axs[i][j].set_ylim([-1, 16])
            axs[i][j].set_xlim([-1, 16])
            axs[i][j].set_xlabel('x')
            axs[i][j].set_ylabel('y')
            ms.grouping_iteration(data=data)
    plt.show()

main()