import numpy as np

def distribution_characteristics(df, var_name):
    print(var_name+' mean : '+str(df[var_name].mean()))
    print(var_name+' median : '+str(df[var_name].median()))
    print(var_name+' stddev : '+str(df[var_name].std()))

def plot_histogram(df, x_lab, ax, unit):
    ax.hist(x=df[x_lab], bins='auto', alpha=0.7, rwidth=0.85)
    ax.grid(axis='y', alpha=0.75)
    ax.set_xlabel('Value('+unit+')')
    ax.set_ylabel('Frequency')
    ax.set_title(x_lab.capitalize() + ' histogram')

def print_formatted_clusters(countries, labels, n):
    label_sizes = [np.count_nonzero(labels == l) for l in set(labels)]
    [print('Cluster {0}\n {1}\n'
    .format(l, countries[labels == l]
    .sample(n=n if n < s else s))) for l, s in zip(set(labels), label_sizes)]